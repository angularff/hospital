import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {LoginService} from '../../../auth/services/login/login.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientsGuardServiceService implements CanActivate {
  constructor(private loginService: LoginService) {
  }

  // tslint:disable-next-line:max-line-length
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.loginService.isLoggedIn();
  }
}
