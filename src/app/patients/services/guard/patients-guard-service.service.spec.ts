import { TestBed } from '@angular/core/testing';

import { PatientsGuardServiceService } from './patients-guard-service.service';

describe('PatientsGuardServiceService', () => {
  let service: PatientsGuardServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientsGuardServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
