import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CheckInComponent} from './components/check-in/check-in.component';
import {CheckOutComponent} from './components/check-out/check-out.component';
import {PatientsGuardServiceService} from './services/guard/patients-guard-service.service';

const routes: Routes = [
  {path: 'check-in', component: CheckInComponent, canActivate: [PatientsGuardServiceService]},
  {path: 'check-out', component: CheckOutComponent, canActivate: [PatientsGuardServiceService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule {
}
