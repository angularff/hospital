import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.css']
})
export class CheckInComponent implements OnInit {
  patientCheckedIn: boolean = null;

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(checkInForm: NgForm) {
    const cnp = checkInForm.value.cnp;
    const patients = JSON.parse(localStorage.getItem('patients')) || {};
    const existingPatient = patients[cnp];
    this.patientCheckedIn = !!existingPatient;
    if (this.patientCheckedIn) {
      return;
    }

    patients[cnp] = JSON.stringify(checkInForm.value);
    localStorage.setItem('patients', JSON.stringify(patients));
    checkInForm.resetForm();
  }
}
