import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  readonly patients;

  constructor() {
    this.patients = JSON.parse(localStorage.getItem('patients'), ((key, value) => key !== '' ? JSON.parse(value) : value)) || {};
  }

  get hasPatients(): boolean {
    return Object.keys(this.patients).length !== 0;
  }

  ngOnInit(): void {
  }

  checkOut(cnp) {
    delete this.patients[cnp];
    localStorage.setItem('patients', JSON.stringify(this.patients));
  }
}
