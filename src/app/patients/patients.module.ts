import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {PatientsRoutingModule} from './patients-routing.module';
import {CheckInComponent} from './components/check-in/check-in.component';
import {CheckOutComponent} from './components/check-out/check-out.component';


@NgModule({
  declarations: [CheckInComponent, CheckOutComponent],
  imports: [
    CommonModule,
    PatientsRoutingModule,
    FormsModule,
  ],
  exports: [
    CheckInComponent,
    CheckOutComponent
  ]
})
export class PatientsModule {
}
