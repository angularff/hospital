import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ModalModule} from 'ngx-bootstrap/modal';
import {AuthModule} from './auth/auth.module';
import {RouterModule} from '@angular/router';
import {PatientsModule} from './patients/patients.module';
import {ErrorPagesModule} from './error-pages/error-pages.module';
import {HomeModule} from './home/home.module';
import {HttpClientModule} from '@angular/common/http';
import {fakeBackendProvider} from './shared/services/fake-backend/fake-bakend.service';
import {DashboardModule} from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot([]),
    AuthModule,
    HomeModule,
    DashboardModule,
    PatientsModule,
    HttpClientModule,
    ErrorPagesModule // should always be the last one in the list
  ],
  providers: [fakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule {
}
