import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {PasswordCheckService, PasswordStrength} from '../../services/password-check/password-check.service';
import {DataService} from '../../../shared/services/data-service/data.service';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(private passwordCheckService: PasswordCheckService, private data: DataService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    if (this.isValid(form)) {
      const {username, password, email, phone, city, county, zip} = form.value;
      const user = new User(0, username, password, email, phone, city, county, zip);
      this.data.register(user).toPromise().then(
        _ => this.router.navigate(['login']),
        e => console.log(e.error.message)
      );
    }
  }

  isValid(form: NgForm) {
    return this.isValidPassword(form.value.password) && this.isValidUsername(form.value.username);
  }

  isValidPassword(password: string) {
    return this.passwordCheckService.checkPasswordStrength(password) > PasswordStrength.Common;
  }

  isValidUsername(username: string) {
    return username.length > 4;
  }
}
