import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataService} from '../../../shared/services/data-service/data.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {LoginService} from '../../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private data: DataService, private location: Location, private router: Router, private loginService: LoginService) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    const {username, password} = form.value;
    const user = {username, password};
    this.data.login(user).toPromise().then(
      res => {
        this.loginService.setLoggedInUser(res.user);
        this.router.navigate(['dashboard']);
      },
      e => console.log(e.error.message)
    );
  }
}
