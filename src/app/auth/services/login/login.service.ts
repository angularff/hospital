import {Injectable} from '@angular/core';
import {User} from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly key = 'loggedInUser';
  private user: User;

  constructor() {
    const storedUser = localStorage.getItem(this.key);
    if (!!storedUser) {
      this.user = JSON.parse(storedUser);
    } else {
      this.user = null;
    }
  }

  isLoggedIn(): boolean {
    return !!this.user;
  }

  getLoggedInUser(): User {
    return this.user;
  }

  setLoggedInUser(user: User) {
    if (!user) {
      localStorage.removeItem(this.key);
    } else {
      localStorage.setItem(this.key, JSON.stringify(user));
      this.user = user;
    }
  }
}
