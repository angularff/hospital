import {Injectable} from '@angular/core';

// Password strengths
export const enum PasswordStrength {
  Short = 0,
  Common = 1,
  Weak = 2,
  Ok = 3,
  Strong = 4,
}

@Injectable({
  providedIn: 'root'
})

export class PasswordCheckService {

  public static get MinimumLength(): number {
    return 5;
  }

  // Regex to check for a common password string - all based on 5+ length passwords
  private commonPasswordPatterns = /passw.*|12345.*|09876.*|qwert.*|asdfg.*|zxcvb.*|footb.*|baseb.*|drago.*/;


  // Checks if the given password matches a set of common password
  public isPasswordCommon(password: string): boolean {
    return this.commonPasswordPatterns.test(password);
  }

  // Returns the strength of the current password
  public checkPasswordStrength(password: string): PasswordStrength {

    // Build up the strength of our password
    let numberOfElements = 0;
    numberOfElements = /.*[a-z].*/.test(password) ? ++numberOfElements : numberOfElements;      // Lowercase letters
    numberOfElements = /.*[A-Z].*/.test(password) ? ++numberOfElements : numberOfElements;      // Uppercase letters
    numberOfElements = /.*[0-9].*/.test(password) ? ++numberOfElements : numberOfElements;      // Numbers
    numberOfElements = /[^a-zA-Z0-9]/.test(password) ? ++numberOfElements : numberOfElements;   // Special characters (inc. space)

    let currentPasswordStrength: PasswordStrength;

    // Check then strength of this password using some simple rules
    if (password === null || password.length < PasswordCheckService.MinimumLength) {
      currentPasswordStrength = PasswordStrength.Short;
    } else if (this.isPasswordCommon(password) === true) {
      currentPasswordStrength = PasswordStrength.Common;
    } else if (numberOfElements === 0 || numberOfElements === 1 || numberOfElements === 2) {
      currentPasswordStrength = PasswordStrength.Weak;
    } else if (numberOfElements === 3) {
      currentPasswordStrength = PasswordStrength.Ok;
    } else {
      currentPasswordStrength = PasswordStrength.Strong;
    }

    return currentPasswordStrength;
  }
}
