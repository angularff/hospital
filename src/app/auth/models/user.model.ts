export class User {
  public constructor(private _id: number, private _username: string, private _password: string, private _email: string,
                     private _phone: string, private _city: string, private _county: string, private _zip: string) {
  }

  get id(): number {
    return this._id;
  }

  get username(): string {
    return this._username;
  }

  get password(): string {
    return this._password;
  }

  get email(): string {
    return this._email;
  }

  get phone(): string {
    return this._phone;
  }

  get city(): string {
    return this._city;
  }

  get county(): string {
    return this._county;
  }

  get zip(): string {
    return this._zip;
  }
}
