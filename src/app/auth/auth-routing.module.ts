import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {AuthGuardService} from './services/guard/auth-guard.service';

const routes: Routes = [
  // {path: 'login',  loadChildren: () => import('./components/login/login.component').then(m => m.LoginComponent)},
  // {path: 'register',  loadChildren: () => import('./components/register/register.component').then(m => m.RegisterComponent)},
  {path: 'login', component: LoginComponent, canActivate: [AuthGuardService]},
  {path: 'register', component: RegisterComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
