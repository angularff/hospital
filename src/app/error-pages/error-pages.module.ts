import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ErrorPagesRoutingModule} from './error-pages-routing.module';


@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    ErrorPagesRoutingModule
  ]
})
export class ErrorPagesModule {
}
