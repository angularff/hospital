import { TestBed } from '@angular/core/testing';

import { FakeBackendInterceptor } from './fake-bakend.service';

describe('FakeBackendInterceptor', () => {
  let service: FakeBackendInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeBackendInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
