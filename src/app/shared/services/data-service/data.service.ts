import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../../auth/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = 'http://fake-backend.com/';
  private options = {
    headers: {
      Authentication: 'Bearer fake-jwt-token'
    }
  };

  constructor(private httpClient: HttpClient) {
  }

  getUsers() {
    return this.httpClient.get<User[]>(this.apiUrl + 'users', this.options);
  }

  login(user: { username: string, password: string }) {
    return this.httpClient.post<{ user: User, token: string }>(this.apiUrl + 'users/authenticate', user);
  }

  register(user: User) {
    return this.httpClient.post(this.apiUrl + 'users/register', user);
  }
}
